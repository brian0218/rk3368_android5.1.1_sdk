ifeq ($(strip $(FOTA_UPDATE_SUPPORT)), true)
LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := FotaUpdate
LOCAL_MODULE_TAGS := optional
ifeq ($(strip $(FOTA_UPDATE_WITHOUT_MENU)), true)
LOCAL_SRC_FILES := ./noMenu/FotaUpdate.apk
else
ifeq ($(strip $(FOTA_UPDATE_WITH_ICON)), true)
LOCAL_SRC_FILES := ./withIcon/FotaUpdate.apk
else
LOCAL_SRC_FILES := ./noIcon/FotaUpdate.apk
endif
endif
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_CERTIFICATE := platform
include $(BUILD_PREBUILT)
endif
