#!/system/bin/sh

MARK=/data/local/symbol_netxeon_app_preinstalled
PKGS=/system/preinstall_netxeon
PKGS_DEL=/system/preinstall_netxeon_del

if [ ! -e $MARK ]; then


echo "booting the first time, so pre-install some APKs."

busybox1.11 find $PKGS -name "*\.apk" -exec sh /system/bin/pm install {} \;
busybox1.11 find $PKGS_DEL -name "*\.apk" -exec sh /system/bin/pm install {} \;
busybox1.11 rm -rf $PKGS_DEL/*.apk

busybox1.11 unzip -q -o $PKGS/xbmc-addons.zip -d /sdcard

touch $MARK
echo "OK, installation complete."
fi