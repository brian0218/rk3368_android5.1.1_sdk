#
# Copyright 2014 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

$(call inherit-product, $(LOCAL_PATH)/rk3368.mk)

PRODUCT_NAME := rk3368_box
PRODUCT_DEVICE := rk3368_box
PRODUCT_MODEL := Orion R68

## @sz.
BUILD_WITH_GOOGLE_MARKET ?= true


## @sz. vendor software version.
PRODUCT_PROPERTY_OVERRIDES += ro.vendor.sw.version=100L1

## @sz. hide navigation bar button enable.
PRODUCT_PROPERTY_OVERRIDES += sys.status.hidebar_enable=true

## @sz. for hide navigation bar.
PRODUCT_PROPERTY_OVERRIDES += persist.sys.hideNavigationBar=true


## @sz_add_begin. copy preinstall apk.
###########################################################
## Find all of the apk files under the named directories.
## Meant to be used like:
##    SRC_FILES := $(call all-apk-files-under,src tests)
###########################################################
define all-apk-files-under
$(patsubst ./%,%, \
  $(shell cd $(LOCAL_PATH)/$(1) ; \
          find ./ -maxdepth 1  -name "*.apk" -and -not -name ".*") \
)
endef

## copy preinstall_netxeon apk.
COPY_APK_TARGET := $(call all-apk-files-under,rk3368_box/netxeon/netxeon-preinstall/preinstall_netxeon)
PRODUCT_COPY_FILES += $(foreach apkName, $(COPY_APK_TARGET), \
	$(addprefix $(LOCAL_PATH)/rk3368_box/netxeon/netxeon-preinstall/preinstall_netxeon/, \
	$(apkName)):$(addprefix system/preinstall_netxeon/, $(apkName)))

## copy preinstall_netxeon_del apk.
COPY_APK_TARGET := $(call all-apk-files-under,rk3368_box/netxeon/netxeon-preinstall/preinstall_netxeon_del)
PRODUCT_COPY_FILES += $(foreach apkName, $(COPY_APK_TARGET), \
	$(addprefix $(LOCAL_PATH)/rk3368_box/netxeon/netxeon-preinstall/preinstall_netxeon_del/, \
	$(apkName)):$(addprefix system/preinstall_netxeon_del/, $(apkName)))
## @sz_add_end. copy preinstall apk.

## @sz. copy preinstall_netxeon.sh
PRODUCT_COPY_FILES += $(LOCAL_PATH)/rk3368_box/netxeon/netxeon-preinstall/preinstall_netxeon.sh:system/bin/preinstall_netxeon.sh

## @sz. copy xbmc-addons.zip
PRODUCT_COPY_FILES += $(LOCAL_PATH)/rk3368_box/netxeon/netxeon-preinstall/preinstall_netxeon/xbmc-addons.zip:system/preinstall_netxeon/xbmc-addons.zip


PRODUCT_AAPT_CONFIG := normal tvdpi hdpi
PRODUCT_AAPT_PREF_CONFIG := tvdpi

## @sz.
PRODUCT_COPY_FILES += $(LOCAL_PATH)/rk3368_box/netxeon/keylayout/Vendor_248a_Product_a001.kl:system/usr/Vendor_248a_Product_a001.kl
PRODUCT_COPY_FILES += $(LOCAL_PATH)/rk3368_box/netxeon/keylayout/Vendor_0c45_Product_1109.kl:system/usr/Vendor_0c45_Product_1109.kl

## @sz. Add a busybox. It's a copy of rk busybox1.11. ISO video needs it to be named as busybox.
## We don't rename busybox1.11 to busybox because busybox1.11 maybe needed by other programs.
PRODUCT_COPY_FILES += $(LOCAL_PATH)/rk3368_box/netxeon/busybox/busybox:system/bin/busybox

## @sz. bootanimation.
PRODUCT_COPY_FILES += $(LOCAL_PATH)/rk3368_box/netxeon/bootanimation.zip:system/media/bootanimation.zip

## @sz. 
PRODUCT_BRAND := Tronsmart

#fota start
PRODUCT_PACKAGES += FotaUpdate \
    FotaUpdateReboot
PRODUCT_COPY_FILES += packages/apps/FotaUpdateApp/fotabinder:system/bin/fotabinder
#fota end

## @sz. for bitstream.
PRODUCT_PROPERTY_OVERRIDES += ro.support.lossless.bitstream = true
