#:!/bin/sh

echo "*******************************"
echo     "begin to make "
echo "*******************************"
source java1.7.sh
source build/envsetup.sh
lunch rk3368_box-user

make installclean -j8
make -j8

./mkimage.sh ota

cp kernel/kernel.img rockdev/Image-rk3368_box
cp kernel/resource.img rockdev/Image-rk3368_box

echo "*******************************"
echo     "finish"
echo "*******************************"

